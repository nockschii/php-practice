FROM php:8.3-fpm-alpine
WORKDIR /var/www/php-practice/
RUN curl -sSLf \
        -o /usr/local/bin/install-php-extensions \
        https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions && \
        chmod +x /usr/local/bin/install-php-extensions && \
    install-php-extensions \
        @composer
RUN apk update && apk upgrade \
    && apk add bash
ENV PHP_MEMORY_LIMIT=128M
COPY . .
CMD ["php-fpm"]