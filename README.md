# PHP practice

example project setup with: - composer
_ phpunit
_ mockery

start up containers and run on background by `docker-compose up -d`

go inside php container

```
docker exec -it php bash                    (or sh)
```

run all tests

```
./vendor/bin/phpunit test
```

run specific test

```
./vendor/bin/phpunit test --filter=ExampleClassTest
```

run tests without entering container

```
docker-compose exec -w /var/www/php-practice/ php {command}
docker-compose exec -w /var/www/php-practice/ php composer install
docker-compose exec -w /var/www/php-practice/ php ./vendor/bin/phpunit test
docker-compose exec -w /var/www/php-practice/ php ./vendor/bin/phpunit test --filter=ExampleClassTest
```
