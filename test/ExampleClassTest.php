<?php

namespace AppTests;

use App\ExampleClass;
use PHPUnit\Framework\TestCase;

class ExampleClassTest extends TestCase
{
    /** @test */
    public function shouldReturnHello()
    {
        $exampleClass = new ExampleClass();

        $this->assertEquals("hello", $exampleClass->sayHello());
    }
}